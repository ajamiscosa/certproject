import java.util.ArrayList;
import java.util.HashMap;

public class Inventory {
    private HashMap<Integer, Product> productsMap;
    private HashMap<Integer, Integer> stocksMap;

    /**
     * Creates a new instance of Inventory
     */
    public Inventory() {
        productsMap = new HashMap<Integer, Product>();
        stocksMap = new HashMap<Integer, Integer>();
    }

    /**
     * Adds an instance of {@code Product} to the {@code Inventory}
     * 
     * @param product
     * @param initialQuantity
     * 
     * @throws InventoryAppException
     * @throws IllegalArgumentException
     */
    public void addProduct(Product product, int initialQuantity) throws InventoryAppException, IllegalArgumentException {
        if(initialQuantity < 0) throw new IllegalArgumentException("Initial quantity cannot be less than 0.");
        if(product == null) throw new InventoryAppException("Cannot have a null value for Product.");

        productsMap.put(product.getProductId(), product);
        stocksMap.put(product.getProductId(), initialQuantity);
    }

    /**
     * Returns an {@code ArrayList} of all {@code Product}
     * 
     * @return the {@code ArrayList} containing all {@code Product}
     */
    public ArrayList<Product> getAllProducts() {
        return new ArrayList<Product>(productsMap.values());
    }
    
    /**
     * Returns the {@code currentStock} available of the product
     * 
     * @return  the current available stock of the product
     */
    public int getCurrentStock(int productId) throws InventoryAppException {
        if(!stocksMap.containsKey(productId)) throw new InventoryAppException(String.format("Product with id [%s] doesn't exist in the inventory.", productId));
        return stocksMap.get(productId);
    }

    /**
     * Find the {@code Product} with the specified {@code productId} from the {@code Inventory}
     * 
     * @param productId the id of the {@code Product}
     * @return the instance of the {@code Product} matching the {@code productId}
     * @throws InventoryAppException thrown when the specified {@code productId} doesn't exist in the {@code Inventory}
     */
    public Product findProduct(int productId) throws InventoryAppException {
        if(!productsMap.containsKey(productId)) throw new InventoryAppException(String.format("Product with id [%s] doesn't exist in the inventory.", productId));
        return productsMap.get(productId);
    }

    /**
     * Adds additional stock {@code quantity} of the specified {@code Product}
     * 
     * @param product the {@code Product} to increase stocks
     * @param quantity the {@code quantity} to add
     */
    public void restock(Product product, int quantity) {
        int currentQty = stocksMap.get(product.getProductId());
        currentQty += quantity;
        stocksMap.put(product.getProductId(), currentQty);
    }

    /**
     * Update the specified {@code Product} with the requested {@code quantity}
     * 
     * @param product the {@code Product} being transacted
     * @param quantity the number of {@code Product} being transacted
     */
    public void checkout(Product product, int quantity) throws InventoryAppException {
        int currentQty = stocksMap.get(product.getProductId());
        currentQty -= quantity;
        if(currentQty < 0) {
            throw new InventoryAppException("Requested quantity is above current available stocks.");
        }
        else {
            stocksMap.put(product.getProductId(), currentQty);
        }
    }
}