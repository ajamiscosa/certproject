import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class TransactionManager {
    private HashMap<Integer, Transaction> transactionsMap;
    private Inventory inventory;

    /**
     * Creates a new instance of {@code TransactionManager}
     */
    private TransactionManager() {
        transactionsMap = new HashMap<Integer, Transaction>();
    }

    /**
     * Creates a new instance of {@code TransactionManager} for the specified instance of {@code Inventory}
     * 
     * @param inventory the instance of {@code Inventory} to do transactions against
     */
    public TransactionManager(Inventory inventory) {
        this();
        this.inventory = inventory;
    }

    /**
     * Find the {@code Transaction} with the specified {@code transactionId} from the {@code Transaction}
     * 
     * @param transactionId the id of the {@code Transaction}
     * @return the instance of the {@code Transaction} matching the {@code transactionId}
     * @throws InventoryAppException thrown when the specified {@code transactionId} doesn't exist in the {@code Inventory}
     */
    public Transaction findTransaction(int transactionId) throws InventoryAppException {
        if(!transactionsMap.containsKey(transactionId)) throw new InventoryAppException(String.format("Transaction with id [%s] doesn't exist in the inventory.", transactionId));
        return transactionsMap.get(transactionId);
    }

    /**
     * Returns the total running sales of all made {@code Transaction}
     * 
     * @return the total amount in {@code BigDecimal}
     */
    public BigDecimal getTotalSalesAmount() {
        BigDecimal total = BigDecimal.ZERO;
        for(Transaction transaction : transactionsMap.values()) {
            total = total.add(transaction.getTotalAmount());
        }
        return total;
    }

    /**
     * Add the instance of {@code Transaction} to the list
     * 
     * @param transaction the {@code Transaction}
     * @throws InventoryAppException thrown if the instance of {@code Transaction} passed is {@code null}
     */
    public void addTransaction(Transaction transaction) throws InventoryAppException {
        if(transaction == null) throw new InventoryAppException("Cannot have a null value for Transaction.");

        transactionsMap.put(transaction.getTransactionId(), transaction);

        for(Product product : transaction.getLineItems()) {
            inventory.checkout(product, transaction.getQuantity(product.getProductId()));
        }

        transaction = null;
    }

    /**
     * Returns an {@code ArrayList} of all {@code Transaction}
     * 
     * @return the {@code ArrayList} containing all {@code Transaction}
     */
    public ArrayList<Transaction> getAllTransactions() {
        return new ArrayList<Transaction>(transactionsMap.values());
    }
}
