import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
public class InventoryApp {
    private static final String FILLER = String.format("+%s+\n", "-".repeat(50));

    private Scanner scanner = new Scanner(System.in);
    private Inventory inventory;
    private Transaction transaction;
    private TransactionManager transactionManager;

    public InventoryApp() {
        inventory = new Inventory();
        transactionManager = new TransactionManager(inventory);

        try {
            inventory.addProduct(new Product("Pen", 5), 10);
            inventory.addProduct(new Product("Pencil", 2), 20);
            inventory.addProduct(new Product("Notebook", 8), 10);
            inventory.addProduct(new Product("Eraser", 2), 30);
        } catch(IllegalArgumentException | InventoryAppException ex ) { // Since Java7 we are able to catch multiple Exception types in a single catch block.
            System.out.printf("Unable to start application: %s\n", ex.getMessage());
            Utilities.pressAnyKey(scanner);
            System.exit(-1);
        }
    }

    /**
     * Display the Main Screen of the Application
     */
    public void displayMainScreen() {
        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText("Inventory System", 50));
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", "1. Display Inventory");
        System.out.printf("| %-48s |\n", "2. Add Product Quantity");
        System.out.printf("| %-48s |\n", "3. Enter Sales Transaction");
        System.out.printf("| %-48s |\n", "4. Display All Sales Transactions");
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", "0. Exit Application");
        System.out.print(FILLER);

        int input = displayInputPrompt("Enter Input:");
        doAction(input);
    }

    /**
     * All purpose method used to display a user-specific prompt that expects an input
     * 
     * @param promptMessage the message to be displayed
     * @return the captured input of the user
     */
    private int displayInputPrompt(String promptMessage) {
        System.out.printf("> %s ", promptMessage);

        int response = -1;
        
        try {
            response = scanner.nextInt();
            
            if(response < 0) {
                System.out.printf("%s\n", "You have entered an invalid input.");
                System.out.printf("%s\n", "Please try again");
                Utilities.pressAnyKey(scanner);
            }
        } catch(InputMismatchException e) {
            System.out.printf("%s\n", "You have entered an invalid input.");
            System.out.printf("%s\n", "Please try again");
            Utilities.pressAnyKey(scanner);
        }
        
        return response;
    }

    private void doAction(int input) {
        switch(input) {
            case 1: 
                displayProductInventory();
                break;
            case 2: 
                displayAddProductQuantity();
                break;
            case 3: 
                displayNewSalesTransaction();
                break;
            case 4: 
                displayAllSalesTransactions();
                break;
            case 0:
                exitApplication();
                break;
            default:
                displayMainScreen();
                break;
        }
    }

    /**
     * Display the list of products
     */
    private void displayProductInventory() {
        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText("Product Inventory", 50));
        System.out.print(FILLER);
        printInventory(true);
        System.out.print(FILLER);
        Utilities.pressAnyKey(scanner);
        displayMainScreen();
    }

    /**
     * Display Add Product Quantity initial screen
     */
    private void displayAddProductQuantity() {
        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText("Product Inventory", 50));
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", "0 - Exit to Main Menu");
        System.out.print(FILLER);
        System.out.printf("| %-10s | %-20s | %12s |\n", "ID", "Name", "Quantity");
        System.out.print(FILLER);

        try {
            for(Product product : inventory.getAllProducts()) {
                System.out.printf( 
                    "| %-10d | %-20s | %12d |\n", 
                    product.getProductId(),
                    product.getName(),
                    inventory.getCurrentStock(product.getProductId())
                );
            }
        } catch(InventoryAppException ex) {
            System.out.printf("%s\n", ex.getMessage());
            System.out.printf("%s\n", "Please try again");
            Utilities.pressAnyKey(scanner);
        }
        
        System.out.print(FILLER);

        int input = displayInputPrompt("Select Product ID to update:");
        displayAddProductQuantity_sub(input);
    }

    /**
     * A sub-routine in routing the actions based on the user input
     * 
     * @param input user input from the prompt
     */
    private void displayAddProductQuantity_sub(int input) {
        switch(input) {
            case -1: 
                displayAddProductQuantity();
                break;
            case 0:
                displayMainScreen();
                break;
            default: {
                try {
                    Product product = inventory.findProduct(input);
                    showUpdateProductQuantyScreen(product);
                }
                catch(InventoryAppException ex) {
                    System.out.printf("%s\n", ex.getMessage());
                    System.out.printf("%s\n", "Please try again");
                    Utilities.pressAnyKey(scanner);
                    displayAddProductQuantity();
                }
            }
            break;
        }
    }

    /**
     * Displays prompt for updating {@code Product} quantity
     * 
     * @param product the product to be updated
     */
    private void showUpdateProductQuantyScreen(Product product) {
        Utilities.clearScreen();
        try {
            System.out.print(FILLER);
            System.out.printf("|%s|\n", Utilities.centeredText("Updating Product", 50));
            System.out.print(FILLER);
            System.out.printf("| %-48s |\n", "0 - Cancel Update");
            System.out.print(FILLER);
            System.out.printf("| %-48s |\n", String.format("%s / %d pc(s)", product.getName(), inventory.getCurrentStock(product.getProductId())));
            System.out.print(FILLER);

            int input = displayInputPrompt(String.format("Enter quantity to add to [%s]:", product.getName()));
            
            if(input == 0) {
                displayAddProductQuantity();
            }
            else if(input < 0) {
                showUpdateProductQuantyScreen(product);
            }
            else {
                inventory.restock(product, input);
                showUpdateProductQuantityComplete(product, input);
            }
        } catch(InventoryAppException ex) {
            System.out.printf("| %-48s |\n", ex.getMessage());
            System.out.print(FILLER);
            System.out.printf("%s\n", "Please try again");
            Utilities.pressAnyKey(scanner);
            displayAddProductQuantity();
        }
    }

    /**
     * Displays a confirmation message that the {@code Product} quantity has been updated
     * 
     * @param product the {@code Product} being updated
     * @param quantity the number of items
     */
    private void showUpdateProductQuantityComplete(Product product, int quantity) {
        try {
            int oldStock = inventory.getCurrentStock(product.getProductId());
            int newStock = oldStock - quantity;

            Utilities.clearScreen();
            System.out.print(FILLER);
            System.out.printf("|%s|\n", Utilities.centeredText("Quantity Updated", 50));
            System.out.print(FILLER);
            System.out.printf ("| %-48s |\n", String.format("%s %s", product.getName(),  String.format("+(%d)", quantity)));
            System.out.printf ("| %-48s |\n", String.format("%d -> %d", newStock, oldStock));
            System.out.print(FILLER);
            Utilities.pressAnyKey(scanner);
            displayAddProductQuantity();
        } catch(InventoryAppException ex) {
            System.out.print(FILLER);
            System.out.printf("| %-48s |\n", ex.getMessage());
            System.out.print(FILLER);
            Utilities.pressAnyKey(scanner);
            displayAddProductQuantity();
        }
    }

    /**
     * Display New Sales Transaction initial screen
     */
    private void displayNewSalesTransaction() {
        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText("Available Products", 50));
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", "0 - Exit to Main Menu");
        System.out.print(FILLER);
        printInventory(false);
        System.out.print(FILLER);

        int input = displayInputPrompt("Select Product ID to purchase:");
        if(input == 1 && transaction != null) {
            showAddProductToTransaction();
        }
        else {
            displayNewSalesTransaction_sub(input);
        }
    }

    /**
     * A sub-routine in routing the actions based on the user input
     * 
     * @param input user input from the prompt
     */
    private void displayNewSalesTransaction_sub(int input) {
        switch(input) {
            case -1: 
                displayNewSalesTransaction();
                break;
            case 0:
                displayMainScreen();
                break;
            default: {
                try {
                    Product product = inventory.findProduct(input);
                    showTransactionScreen(product);
                }
                catch(InventoryAppException ex) {
                    System.out.printf("%s\n", ex.getMessage());
                    System.out.printf("%s\n", "Please try again");
                    Utilities.pressAnyKey(scanner);
                    displayNewSalesTransaction();
                }
            }
            break;
        }
    }

    /**
     * Display the {@code Transaction} screen
     * 
     * @param product the {@code Product} being transacted
     */
    private void showTransactionScreen(Product product) {
        if(transaction == null) { // If there is no active Transaction, create new instance
            transaction = new Transaction();
        }
        Utilities.clearScreen();
        try {
            System.out.print(FILLER);
            System.out.printf("|%s|\n", Utilities.centeredText("Purchase Product", 50));
            System.out.print(FILLER);
            System.out.printf("| %-48s |\n", "0 - Cancel Purchase");
            System.out.print(FILLER);
            System.out.printf("| %-48s |\n", String.format("%s / %d pc(s)", product.getName(), this.getAvailableStocks(product.getProductId())));
            System.out.print(FILLER);

            int input = displayInputPrompt(String.format("Enter # of [%s] to purchase:", product.getName()));
            
            if(input == 0) {
                transaction = null;
                displayNewSalesTransaction();
            }
            else if(input < 0) {
                showTransactionScreen(product);
            }
            else {
                if(this.getAvailableStocks(product.getProductId()) < input) {
                    System.out.println("Insufficient stocks for puchase.");
                    System.out.printf("%s\n", "Please try again");
                    Utilities.pressAnyKey(scanner);
                    showTransactionScreen(product);
                }
                else {
                    transaction.addProduct(product, input);
                    showAddProductToTransaction();
                }
            }
        } catch(InventoryAppException ex) {
            System.out.printf("| %-48s |\n", ex.getMessage());
            System.out.printf("%s\n", "Please try again");
            Utilities.pressAnyKey(scanner);
            showTransactionScreen(product);
        }
    }

    /**
     * Displays the list of {@code Product} currently in the {@code Transaction}
     */
    private void showAddProductToTransaction() {
        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText("Line Items", 50));
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", "0 - Cancel Purchase");
        System.out.print(FILLER);
        System.out.printf("| %-4s | %-15s | %10s | %10s |\n", "ID", "Name", "Quantity", "Sub-total");

        for(Product lineItem : transaction.getLineItems()) {
            BigDecimal subTotal = lineItem.getPrice().multiply(new BigDecimal(transaction.getQuantity(lineItem.getProductId())));
            
            System.out.printf("| %-4s | %-15s | %10s | %10s |\n", 
                lineItem.getProductId(), 
                lineItem.getName(), 
                transaction.getQuantity(lineItem.getProductId()), 
                Utilities.convertToCurrency(subTotal)
            );
        }

        System.out.print(FILLER);
        System.out.printf("| %48s |\n", String.format("Total: %s", Utilities.convertToCurrency(transaction.getTotalAmount())));
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", "1 - Add Another Product");
        System.out.printf("| %-48s |\n", "2 - Complete Transaction");
        System.out.print(FILLER);
        int input = displayInputPrompt("Enter Option:");
        
        if(input == 0) { // 0 = Cancel Transaction
            transaction = null; // set to null since Transaction is cancelled
            displayNewSalesTransaction();
        }
        else if(input < 0) {
            showAddProductToTransaction();
        }
        else if(input == 1) {
            displayNewSalesTransaction();
        }
        else if(input == 2) {
            showCompletedTransaction();
        }
        else {
            System.out.printf("%s\n", "You have entered an invalid input.");
            System.out.printf("%s\n", "Please try again");
            Utilities.pressAnyKey(scanner);
            showAddProductToTransaction();
        }
    }

    /**
     * Displays a confirmation message that the {@code Transaction} has is complete
     */
    private void showCompletedTransaction() {
        try {
            transactionManager.addTransaction(transaction);
        } catch(InventoryAppException ex) {
            System.out.printf("%s\n", ex.getMessage());
            System.out.printf("%s\n", "Please try again");
            Utilities.pressAnyKey(scanner);
            displayNewSalesTransaction();
        }

        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", 
            Utilities.centeredText(
                String.format("Transaction [%s] Completed", transaction.getTransactionId()), 50
            )
        );
        System.out.print(FILLER);
        System.out.printf("| %-48s |\n", String.format("# of Products: %s", transaction.getLineItems().size()));
        System.out.printf("| %-48s |\n", String.format("Total Amount: %s", Utilities.convertToCurrency(transaction.getTotalAmount())));
        System.out.print(FILLER);
        
        transaction = null; // set to null after successful Transaction
        
        Utilities.pressAnyKey(scanner);
        displayMainScreen();
    }

    /**
     * Displays the {@code Transaction} info
     * 
     * @param transaction the {@code Transaction} to display
     */
    private void showTransactionInfo(Transaction transaction) {
        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText(String.format("Transaction [%s]", transaction.getTransactionId()), 50));
        System.out.print(FILLER);
        System.out.printf("| %-4s | %-15s | %10s | %10s |\n", "ID", "Name", "Quantity", "Sub-total");

        for(Product lineItem : transaction.getLineItems()) {
            BigDecimal subTotal = lineItem.getPrice().multiply(new BigDecimal(transaction.getQuantity(lineItem.getProductId())));
            
            System.out.printf("| %-4s | %-15s | %10s | %10s |\n", 
                lineItem.getProductId(), 
                lineItem.getName(), 
                transaction.getQuantity(lineItem.getProductId()), 
                Utilities.convertToCurrency(subTotal)
            );
        }

        System.out.print(FILLER);
        System.out.printf("| %48s |\n", String.format("Total: %s", Utilities.convertToCurrency(transaction.getTotalAmount())));
        System.out.print(FILLER);

        Utilities.pressAnyKey(scanner);
        displayAllSalesTransactions();
    }

    /**
     * Display all {@code Transaction} records
     */
    private void displayAllSalesTransactions() {
        ArrayList<Transaction> transactionList = transactionManager.getAllTransactions();
        int transactionCount = transactionList.size();

        Utilities.clearScreen();
        System.out.print(FILLER);
        System.out.printf("|%s|\n", Utilities.centeredText("All Sales Transaction", 50));

        if(transactionCount > 0) {
            System.out.print(FILLER);
            System.out.printf("| %-48s |\n", "0 - Exit to Main Menu");
        }

        System.out.print(FILLER);
        System.out.printf("| %-15s | %30s |\n", "Transaction ID", "Total Amount");
        System.out.print(FILLER);

        if(transactionCount > 0) {
            for(Transaction transaction : transactionList) {
                System.out.printf(
                    "| %-15d | %30s |\n", 
                    transaction.getTransactionId(),
                    Utilities.convertToCurrency(transaction.getTotalAmount())
                );
            }
            System.out.print(FILLER);
            System.out.printf("| %48s |\n", String.format("Total Sales Amount: %s", Utilities.convertToCurrency(transactionManager.getTotalSalesAmount())));
            System.out.print(FILLER);

            int input = displayInputPrompt("Select Transaction ID to view:");
            displayAllSalesTransactions_sub(input);
        }
        else {
            System.out.printf("|%s|\n", Utilities.centeredText("No transaction(s) on record", 50));
            System.out.print(FILLER);
            Utilities.pressAnyKey(scanner);
            displayMainScreen();
        }
    }

    /**
     * A sub-routine in routing the actions based on the user input
     * 
     * @param input user input from the prompt
     */
    private void displayAllSalesTransactions_sub(int input) {
        switch(input) {
            case -1:
                displayAllSalesTransactions();
                break;
            case 0:
                displayMainScreen();
                break;
            default: {
                try {
                    Transaction transaction = transactionManager.findTransaction(input);
                    showTransactionInfo(transaction);
                }
                catch(InventoryAppException ex) {
                    System.out.printf("%s\n", ex.getMessage());
                    System.out.printf("%s\n", "Please try again");
                    Utilities.pressAnyKey(scanner);
                    displayAllSalesTransactions();
                }
            }
            break;
        }
    }

    /**
     * Sub-routine to display product details with additional flag to
     * include zero-quantity products.
     * 
     * @param includeZeroQuantity flag [true = include / false = exclude]
     */
    private void printInventory(boolean includeZeroQuantity) {
        int availableProductCount = 0;

        if(transaction != null) { // If there is an active Transaction, display Go to Cart menu
            System.out.printf("| %-48s |\n", "1 - Go to Cart");
            System.out.print(FILLER);
        }

        System.out.printf("| %-4s | %-15s | %8s | %12s |\n", "ID", "Name", "Stock", "Price");
        System.out.print(FILLER);

        for(Product product : inventory.getAllProducts()) {
            try {
                int productId = product.getProductId();
                int currentStock = inventory.getCurrentStock(productId);
                int availableStock = getAvailableStocks(productId);

                if((availableStock <= 0 || currentStock <= 0) && !includeZeroQuantity) {
                    continue;
                }

                System.out.printf( 
                    "| %-4d | %-15s | %8d | %12s |\n", 
                    product.getProductId(),
                    product.getName(),
                    availableStock,
                    Utilities.convertToCurrency(product.getPrice())
                );
                
                availableProductCount++;
            } catch(InventoryAppException ex) {
                System.out.printf("%s\n", ex.getMessage());
                System.out.printf("%s\n", "Please try again");
                Utilities.pressAnyKey(scanner);
            }
        }

        if(availableProductCount == 0) {
            System.out.printf("| %-48s |\n", "No Available Product(s)");
        }
    }

    /**
     * Returns the current available stocks based on actual stock and items currently being transacted.
     * 
     * @param productId id of the {@code Product} to check
     * @return the remaining available quantity
     * @throws InventoryAppException thrown when the indicated {@code productId} doesn't match any {@code Product} in the {@code Inventory}
     */
    private int getAvailableStocks(int productId) throws InventoryAppException {
        try {
            int currentStock = inventory.getCurrentStock(productId);
            int requestedQuantity = 0;
            int availableStock = 0;

            if(transaction != null) {
                requestedQuantity = transaction.getQuantity(productId);
            }
            
            availableStock = currentStock - requestedQuantity;

            return availableStock;
        } catch(InventoryAppException ex) {
            ex.printStackTrace();
            Utilities.pressAnyKey(scanner);
        }
        return -1;
    }
    
    /**
     * Exit the Application
     */
    private void exitApplication() {
        Utilities.clearScreen();
        scanner.close();
        System.out.println("Exiting application...");
    }
}