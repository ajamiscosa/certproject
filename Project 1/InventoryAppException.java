public class InventoryAppException extends Exception {
    public InventoryAppException() {
        super("Inventory App Exception: Invalid Action");
    }

    public InventoryAppException(String message) {
        super(String.format("Inventory App Exception: %s", message));
    }
}
