import java.math.BigDecimal;

public class Product {
    private static int uniqueId = 1000;

    private int productId;
    private BigDecimal price;
    private String name;

    private Product() {
        uniqueId++;
    }

    /**
     * Creates a new instance of {@code Product}
     * 
     * @param name name of the product
     * @param price the price of the product in {@code BigDecimal}
     * @param initialStock initial inventory stock
     */
    public Product(String name, BigDecimal price) {
        this();
        this.productId = uniqueId;
        this.name = name;
        this.price = price;
    }

    /**
     * Creates a new instance of {@code Product}
     * 
     * @param name name of the product
     * @param price price of the product in {@code int}
     * @param initialStock initial inventory stock
     */
    public Product(String name, int price) {
        this(name, new BigDecimal(price));
    }

    /**
     * Returns the {@code id} of the {@code Product}
     * 
     * @return  the id of the product
     */
    public int getProductId() {
        return this.productId;
    }

    /**
     * Returns the {@code name} of the product
     * 
     * @return  the name of the product
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the {@code price} of the product
     * 
     * @return  the price of the product
     */
    public BigDecimal getPrice() {
        return this.price;
    }
}
