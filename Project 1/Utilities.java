import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Utilities {
    
    /**
     * Displays a "Press [Enter] to continue..."
     * prompt and expects the user to press {@code Enter} to proceed
     * 
     * @param scanner the {@code Scanner} to be used
     */
    public static void pressAnyKey(Scanner scanner) {
        try{
            System.out.print("Press [Enter] to continue...");
            System.in.read();
            scanner.nextLine();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Clears the screen 
     */
    public static void clearScreen() {
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
    }

    /**
     * Centers a {@code String} on {@code System.out} with a specified text length
     * Ref: https://stackoverflow.com/a/26772035
     * 
     * @param str the {@code String} to display
     * @param size the total length of the {@code String} to be outputted
     * @return the modified {@code String} centered
     */
    public static String centeredText(String str, int size) {
        int left = (size - str.length()) / 2;
        int right = size - left - str.length();
        String repeatedChar = " "; // can be modified to accept other values/characters but currently OOS.
        StringBuffer buff = new StringBuffer();
        for (int i = 0; i < left; i++) {
            buff.append(repeatedChar);
        }
        buff.append(str);
        for (int i = 0; i < right; i++) {
            buff.append(repeatedChar);
        }
        
        return buff.toString();
    }

    /**
     * Converts a {@code BigDecimal} instance into formatted PH currency string
     * 
     * @param bigDecimal the value to format to Peso
     * @return the Peso formatted {@code String}
     */
    public static String convertToCurrency(BigDecimal bigDecimal) {
        return NumberFormat.getCurrencyInstance(new Locale("en", "PH")).format(bigDecimal);
    }
}
