import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class Transaction {
    private static int uniqueId = 1000;
    
    private int transactionId;
    private HashMap<Integer, Product> productMap;
    private HashMap<Integer, Integer> quantityMap;
    private BigDecimal amount;

    /**
     * Creates a new instance of {@code Transaction}
     */
    public Transaction() {
        uniqueId++;

        productMap = new HashMap<Integer, Product>();
        quantityMap = new HashMap<Integer, Integer>();
        transactionId = uniqueId;
    }

    /**
     * Add a {@code Product} to the {@code Transaction} with the specified {@code quantity}
     * 
     * @param product the {@code Product} being transacted
     * @param quantity the number of {@code Product} to be transacted
     */
    public void addProduct(Product product, int quantity) {
        int productId = product.getProductId();
        int qty = 0;

        if(productMap.containsKey(productId)) {
            qty = quantityMap.get(productId);
        }

        qty += quantity;
        
        productMap.put(productId, product);
        quantityMap.put(productId, qty);
    }

    /**
     * Returns the {@code amount} of the {@code Transaction}
     * 
     * @return  the total amount of the transaction
     */
    public BigDecimal getTotalAmount() {
        amount = BigDecimal.ZERO;

        for(Product product: productMap.values()) {
            int quantity = getQuantity(product.getProductId());
            BigDecimal subTotal = product.getPrice().multiply(BigDecimal.valueOf(quantity));
            amount = amount.add(subTotal);
        }
        return amount;
    }

    /**
     * Returns the {@code id} of the {@code Transaction}
     * 
     * @return  the id of the transaction
     */
    public int getTransactionId() {
        return this.transactionId;
    }

    /**
     * Returns the list of {@code Product} included in the {@code Transaction}
     * 
     * @return the list of {@code Product}
     */
    public ArrayList<Product> getLineItems() {
        return new ArrayList<Product>(this.productMap.values());
    }

    /**
     * Returns the quantity of {@code Product} in the {@code Transaction} with the specified {@code productId}
     * 
     * @param productId id of the {@code Product}
     * @return the quantity of the {@code Product} in the {@code Transaction}
     */
    public int getQuantity(int productId) {
        if(this.quantityMap.get(productId) == null) return 0;
        return this.quantityMap.get(productId);
    }
}